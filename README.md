###How to use
There is created postman requests for CRUD operations, please use requests in postman directory

###Mongo db configuration
The application startup file is Api1Application.java. In this example is default mongo db configuration, which means
mongodb is used in localhost and port default.

if your mongodb is in another server, then use that in your spring boot startup file:

```
@EnableEurekaClient
@EnableFeignClients
@EnableCircuitBreaker
@EnableReactiveMongoRepositories
@AutoConfigureAfter(EmbeddedMongoAutoConfiguration.class)
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class Api1Application extends AbstractReactiveMongoConfiguration {

	private final Environment environment;

	public Api1Application(Environment environment) {
		this.environment = environment;
	}

	@Override
	@Bean
	@DependsOn("embeddedMongoServer")
	public MongoClient reactiveMongoClient() {
		Integer port = environment.getProperty("local.mongo.port", Integer.class);
		return MongoClients.create(String.format("mongodb://localhost:%d", port));
	}

	@Override
	protected String getDatabaseName() {
		return "reactive-mongo";
	}


	public static void main(String[] args) {
		SpringApplication.run(Api1Application.class, args);
	}
}
```
There you can write your mongo db server ip address with port.