package com.vk.demo.api1.handler.mocks;

import com.vk.demo.api1.handler.UserHandler;
import com.vk.demo.api1.model.User;
import com.vk.demo.api1.repositories.UserRepository;

import java.util.List;

public class UserHandlerMock {

    public static UserHandler getHandlerWithDataRepository(List<User> users) {
        UserRepository userRepository = new UserRepositoryMock(users);
        return new UserHandler(userRepository);
    }
}
