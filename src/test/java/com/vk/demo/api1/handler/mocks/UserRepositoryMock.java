package com.vk.demo.api1.handler.mocks;

import com.vk.demo.api1.model.User;
import com.vk.demo.api1.repositories.UserRepository;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public class UserRepositoryMock implements UserRepository {

    private List<User> mockUsers;

    public UserRepositoryMock() {
    }

    public UserRepositoryMock(List<User> mockUsers) {
        this.mockUsers = mockUsers;
    }

    @Override
    public Mono<User> findByFirstNameAndLastName(String firstName, String lastName) {
        return Mono.just(mockUsers.iterator().next());
    }

    @Override
    public <S extends User> Mono<S> save(S s) {
        return Mono.just(s);
    }

    @Override
    public <S extends User> Flux<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public <S extends User> Flux<S> saveAll(Publisher<S> publisher) {
        return null;
    }

    @Override
    public Mono<User> findById(String s) {
        if (mockUsers.isEmpty()) return Mono.empty();
        return Mono.just(mockUsers.iterator().next());
    }

    @Override
    public Mono<User> findById(Publisher<String> publisher) {
        return null;
    }

    @Override
    public Mono<Boolean> existsById(String s) {
        return null;
    }

    @Override
    public Mono<Boolean> existsById(Publisher<String> publisher) {
        return null;
    }

    @Override
    public Flux<User> findAll() {
        return Flux.fromIterable(mockUsers);
    }

    @Override
    public Flux<User> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public Flux<User> findAllById(Publisher<String> publisher) {
        return null;
    }

    @Override
    public Mono<Long> count() {
        return null;
    }

    @Override
    public Mono<Void> deleteById(String s) {
        return Mono.when(Mono.just(""));
    }

    @Override
    public Mono<Void> deleteById(Publisher<String> publisher) {
        return null;
    }

    @Override
    public Mono<Void> delete(User user) {
        return null;
    }

    @Override
    public Mono<Void> deleteAll(Iterable<? extends User> iterable) {
        return null;
    }

    @Override
    public Mono<Void> deleteAll(Publisher<? extends User> publisher) {
        return null;
    }

    @Override
    public Mono<Void> deleteAll() {
        return null;
    }
}
