package com.vk.demo.api1.handler;

import com.vk.demo.api1.handler.mocks.UserHandlerMock;
import com.vk.demo.api1.handler.mocks.UserMock;
import com.vk.demo.api1.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@WebFluxTest
@SpringBootTest
public class UserHandlerTest {

    private static final String FIND_ALL_USERS_ENDPOINT = "/api1/findAllUsers";
    private static final String FIND_USER_BY_NAME_ENDPOINT = "/api1/findUserByName/";
    private static final String FIND_USER_BY_NAME_ROUTE_ENDPOINT = "/api1/findUserByName/{firstName}/{lastName}";
    private static final String UPDATE_USER_ROUTE_ENDPOINT = "/api1/updateUser/{id}";
    private static final String UPDATE_USER_ENDPOINT = "/api1/updateUser/";
    private static final String DELETE_USER_ROUTE_ENDPOINT = "/api1/deleteUser/{id}";
    private static final String DELETE_USER_ENDPOINT = "/api1/deleteUser/";
    private static final String SAVE_USER_ENDPOINT = "/api1/saveUser";
    private static final String NEW_USER_ID = "newUserId";
    private static final String OLD_USER_ID = "oldUserId";
    private static final String FIND_USER_BY_ID_ROUTE_ENDPOINT = "/api1/findUserById/{id}";
    private static final String FIND_USER_BY_ID_ENDPOINT = "/api1/findUserById/";
    private WebTestClient webTestClient;
    private static final String expectedEmptyRawJson = "[]";

    @Test
    void findAllUsers_whenHasOneUser_returnUserWithHttpStatus200() {
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(
                Collections.singletonList(UserMock.createUser())
        );
        webTestClient = WebTestClient.bindToRouterFunction(route(GET(FIND_ALL_USERS_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findAllUsers)).build();

        webTestClient.get().uri(FIND_ALL_USERS_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(UserMock.createUserRawJsonArray());
    }

    @Test
    void findAllUsers_whenEmpty_returnEmptyListWithHttpStatus200() {
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.emptyList());
        webTestClient = WebTestClient.bindToRouterFunction(route(GET(FIND_ALL_USERS_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findAllUsers)).build();

        webTestClient.get().uri(FIND_ALL_USERS_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(expectedEmptyRawJson);
    }

    @Test
    void findUserByName() {
        User user = UserMock.createUser();
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.singletonList(user));
        webTestClient = WebTestClient.bindToRouterFunction(route(GET(FIND_USER_BY_NAME_ROUTE_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findUserByName)).build();

        webTestClient.get().uri(FIND_USER_BY_NAME_ENDPOINT + user.getFirstName() + "/" + user.getLastName())
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(UserMock.createUserRawJsonObject());
    }

    @Test
    void findUserById() {
        User user = UserMock.createUser();
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.singletonList(user));
        webTestClient = WebTestClient.bindToRouterFunction(route(GET(FIND_USER_BY_ID_ROUTE_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findUserById)).build();

        webTestClient.get().uri(FIND_USER_BY_ID_ENDPOINT + user.getId())
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(UserMock.createUserRawJsonObject());
    }

    @Test
    void saveUser() {
        User user = UserMock.createUser();
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.singletonList(user));
        webTestClient = WebTestClient.bindToRouterFunction(route(POST(SAVE_USER_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::saveUser)).build();

        webTestClient.post().uri(SAVE_USER_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(user), User.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody().json(UserMock.createUserRawJsonObject());
    }

    @Test
    void updateUser_whenUserByIdExists_updatedUserWithHttpStatus201() {
        User newUser = UserMock.createUserByUniqueId(NEW_USER_ID);
        User oldUser = UserMock.createUserByUniqueId(OLD_USER_ID);
        String updatedUserRawJson = UserMock.createUserRawJsonObject(
                new User(oldUser.getId(), newUser.getFirstName(), newUser.getLastName(), newUser.getAge()));
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.singletonList(oldUser));
        webTestClient = WebTestClient.bindToRouterFunction(route(PUT(UPDATE_USER_ROUTE_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::updateUser)).build();

        webTestClient.put().uri(UPDATE_USER_ENDPOINT + oldUser.getId())
                .body(Mono.just(newUser), User.class)
                .exchange()
                .expectStatus().isCreated()
                .expectBody().json(updatedUserRawJson);
    }

    @Test
    void updateUser_whenUserByIdNotExist_returnHttpStatus404() {
        User newUser = UserMock.createUserByUniqueId(NEW_USER_ID);
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.emptyList());
        webTestClient = WebTestClient.bindToRouterFunction(route(PUT(UPDATE_USER_ROUTE_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::updateUser)).build();

        webTestClient.put().uri(UPDATE_USER_ENDPOINT + OLD_USER_ID)
                .body(Mono.just(newUser), User.class)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void deleteUser_whenUserIdExists_deleteWithHttpStatus204() {
        User user = UserMock.createUser();
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.singletonList(user));
        webTestClient = WebTestClient.bindToRouterFunction(route(DELETE(DELETE_USER_ROUTE_ENDPOINT)
                .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::deleteUser)).build();

        webTestClient.delete().uri(DELETE_USER_ENDPOINT + OLD_USER_ID).exchange().expectStatus().isNoContent();
    }

    @Test
    void deleteUser_whenUserNotExist_returnHttpStatus404() {
        UserHandler userHandler = UserHandlerMock.getHandlerWithDataRepository(Collections.emptyList());
        webTestClient = WebTestClient.bindToRouterFunction(route(DELETE(DELETE_USER_ROUTE_ENDPOINT)
        .and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::deleteUser)).build();

        webTestClient.delete().uri(DELETE_USER_ENDPOINT + OLD_USER_ID).exchange().expectStatus().isNotFound();
    }
}