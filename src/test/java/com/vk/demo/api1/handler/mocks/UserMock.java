package com.vk.demo.api1.handler.mocks;

import com.vk.demo.api1.model.User;

public class UserMock {
    private static final String MOCK_ID = "mockId";
    private static final String MOCK_FIRST_NAME = "mockFirstName";
    private static final String MOCK_LAST_NAME = "mockLastName";
    private static final Integer MOCK_AGE = 10;

    public static String createUserRawJsonArray() {
        return "[{\"id\":" + MOCK_ID + "," +
                "\"firstName\":" + MOCK_FIRST_NAME + "," +
                "\"lastName\":" + MOCK_LAST_NAME + "," +
                "\"age\":" + MOCK_AGE + "}]\n";
    }

    public static String createUserRawJsonObject() {
        return "{\"id\":" + MOCK_ID + "," +
                "\"firstName\":" + MOCK_FIRST_NAME + "," +
                "\"lastName\":" + MOCK_LAST_NAME + "," +
                "\"age\":" + MOCK_AGE + "}\n";
    }

    public static String createUserRawJsonObject(User user) {
        return "{\"id\":" + user.getId() + "," +
                "\"firstName\":" + user.getFirstName() + "," +
                "\"lastName\":" + user.getLastName() + "," +
                "\"age\":" + user.getAge() + "}\n";
    }

    public static User createUser() {
        return new User(MOCK_ID, MOCK_FIRST_NAME, MOCK_LAST_NAME, MOCK_AGE);
    }

    public static User createUserByUniqueId(String uniqueId) {
        return new User (uniqueId, MOCK_FIRST_NAME, MOCK_LAST_NAME, MOCK_AGE);
    }
}
