package com.vk.demo.api1.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@FeignClient("word-count-api")
public interface WordCountApiService {

    @GetMapping("/wordCountApi/countDogs")
    @ResponseBody Integer getDogCount(@RequestParam(value = "list") List<String> list);

    @GetMapping("/wordCountApi/status")
    @ResponseBody String getStatus();
}
