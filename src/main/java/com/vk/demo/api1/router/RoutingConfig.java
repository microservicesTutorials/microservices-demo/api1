package com.vk.demo.api1.router;

import com.vk.demo.api1.handler.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RoutingConfig {

    @Bean
    public RouterFunction<ServerResponse> routeUser(UserHandler userHandler) {
        return route(GET("/api1/findAllUsers").and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findAllUsers)
                .andRoute(GET("/api1/findUserByName/{firstName}/{lastName}").and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findUserByName)
                .andRoute(GET("/api1/findUser/{id}").and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::findUserById)
                .andRoute(POST("/api1/saveUser").and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::saveUser)
                .andRoute(PUT("/api1/updateUser/{id}").and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::updateUser)
                .andRoute(DELETE("/api1/deleteUser/{id}").and(accept(MediaType.APPLICATION_JSON_UTF8)), userHandler::deleteUser);
    }
}
