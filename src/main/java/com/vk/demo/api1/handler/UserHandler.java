package com.vk.demo.api1.handler;

import com.vk.demo.api1.model.User;
import com.vk.demo.api1.repositories.UserRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

@Component
public class UserHandler {

    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String ID = "id";
    private static final String URI_SAVE_USER = "/api1/saveUser/";
    private final UserRepository userRepository;

    public UserHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Mono<ServerResponse> findAllUsers(ServerRequest request) {
        Flux<User> users = userRepository.findAll();
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(users, User.class);
    }

    public Mono<ServerResponse> findUserByName(ServerRequest request) {
        String firstName = request.pathVariable(FIRST_NAME);
        String lastName = request.pathVariable(LAST_NAME);
        return userRepository.findByFirstNameAndLastName(firstName, lastName).flatMap(u -> ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(u), User.class))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> findUserById(ServerRequest request) {
        String id = request.pathVariable(ID);
        return userRepository.findById(id).flatMap(u -> ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8).body(Mono.just(u), User.class))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> saveUser(ServerRequest request) {
        final Mono<User> user = request.bodyToMono(User.class);
        return user.flatMap(u -> userRepository.save(u).flatMap(saved ->
                ServerResponse.created(URI.create(URI_SAVE_USER + saved.getId())).body(Mono.just(saved), User.class)));
    }

    public Mono<ServerResponse> updateUser(ServerRequest request)
    {
        final String oldUserId = request.pathVariable(ID);
        final Mono<User> newUser = request.bodyToMono(User.class);
        return userRepository.findById(oldUserId)
                .flatMap(u -> userRepository.deleteById(oldUserId)
                        .then(newUser.map(d -> new User(oldUserId, d.getFirstName(), d.getLastName(), d.getAge()))
                        .flatMap (userRepository::save)
                        .flatMap(s -> ServerResponse.created(URI.create(URI_SAVE_USER + s.getId()))
                                .body(Mono.just(s), User.class))))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> deleteUser(ServerRequest request) {
        String userId = request.pathVariable(ID);
        return userRepository.findById(userId)
                .flatMap(a -> userRepository.deleteById(userId)
                        .then(ServerResponse.noContent().build()))
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}
